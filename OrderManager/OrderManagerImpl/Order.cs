﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    public class Order : KeyItem
    {
        private static IdFactory ID_FACTORY = new IdFactory();
        private int id;

        public int Id
        { get { return this.id; } }

        private List<Dish> dishes = new List<Dish>();

        public List<Dish> Dishes {
            get { return dishes; }
        }

        protected Order()
        {
            id = ID_FACTORY.GetNextId();
        }

        public int getKey()
        {
            return this.id;
        }

        public int getCost()
        {
            int tot = 0;
            foreach(Dish d in dishes)
            {
                tot += d.Price;
            }
            return tot;
        }

        public Dictionary<Dish, int> getDishCounts()
        {
            Dictionary<Dish, int> counts = new Dictionary<Dish, int>();
            foreach (Dish dish in Dishes)
            {
                counts[dish] = counts[dish] + 1;
            }
            return counts;
        }
    }
}
