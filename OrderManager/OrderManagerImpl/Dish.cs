﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    public class Dish : KeyItem
    {
        public static IdFactory ID_FACTORY = new IdFactory();
        private int id;

        public int Id
        { get { return id; } }

        private String description;
        public String Description {
            get { return description; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Null or white space");
                description = value;
            }
        }

        private int price;
        public int Price {
            get { return price; }
            set
            {
                if (value < 0 || value > 100000)
                    throw new ArgumentOutOfRangeException("Must be in range 0 - 100,000");
                price = value;
            }
        }
        public Boolean Vegetarian { get; set; }

        public Dish(String description, int price, Boolean vegetarian)
        {
            id = ID_FACTORY.GetNextId();
            Description = description;
            Price = price;
            Vegetarian = vegetarian;
        }

        public Dish(int id, String description, int price, Boolean vegetarian)
        {
            this.id = id;
            Description = description;
            Price = price;
            Vegetarian = vegetarian;
        }

        public override string ToString()
        {
            return String.Format("{0} {1:C2} {2}", Description, Price / 100.0, Vegetarian ? "(v)" : "");
        }

        public int getKey()
        {
            return this.id;
        }
    }
}
