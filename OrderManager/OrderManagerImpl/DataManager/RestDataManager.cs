﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OrderManagerImpl.DataManager.DelegateList;

namespace OrderManagerImpl.DataManager
{
    public class RestDataManager : DataManager
    {
        /*
         * Must return 
         */
        private DataManagerList<Dish> dishes;
        public DataManagerList<Dish> Dishes { get { return dishes; } }

        private DataManagerList<Server> servers;
        public DataManagerList<Server> Servers { get { return servers; } }

        private DataManagerList<Driver> drivers;
        public DataManagerList<Driver> Drivers { get { return drivers; } }

        private DataManagerList<Order> orders;
        public DataManagerList<Order> Orders { get { return orders; } }

        public RestDataManager()
        {
            //TODO remove second generic parameter once all have an id
        }

        /*
         * Used for testing purposes only
         */
        public void generateFakeData()
        {
        }


        public void init()
        {
            dishes = new DishRestList();
            servers = new ServerRestList();
            drivers = new DriverRestList();
            orders = new OrderRestList(this);
        }
    }
}
