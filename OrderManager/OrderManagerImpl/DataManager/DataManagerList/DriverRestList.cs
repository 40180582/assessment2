﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;
using System.Net.Http.Headers;

using Newtonsoft.Json;

namespace OrderManagerImpl.DataManager.DelegateList
{
    class DriverRestList : DataManagerList<Driver>
    {
        private const string URL = "http://40180582.pythonanywhere.com/driver/";

        HttpClient client;
        public DriverRestList()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.Timeout = TimeSpan.FromSeconds(5);

            RepopulateList();

        }

        private void RepopulateList()
        {
            HttpResponseMessage response = client.GetAsync("").Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataList = JsonConvert.DeserializeObject(result);
                foreach (dynamic data in dataList)
                {
                    Driver driver = new Driver((string)data.name, (string)data.carRegistration, (int)data.id);
                    list.Add(driver);
                }
            }
        }

        public override void Add(Driver item)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("name", item.Name));
            data.Add(new KeyValuePair<string, string>("carRegistration", item.CarRegistration));
            HttpResponseMessage response = client.PostAsync("", new FormUrlEncodedContent(data)).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataResult = JsonConvert.DeserializeObject(result);
                Driver driver = new Driver((string)dataResult.name, (string)dataResult.carRegistration, (int)dataResult.id);
                list.Add(driver);
            }
        }

        public override void Update(Driver item)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("name", item.Name));
            data.Add(new KeyValuePair<string, string>("carRegistration", item.CarRegistration));
            HttpResponseMessage response = client.PostAsync(item.StaffId.ToString() + "/", new FormUrlEncodedContent(data)).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataResult = JsonConvert.DeserializeObject(result);
                Driver driver = new Driver((string)dataResult.name, (string)dataResult.carRegistration, (int)dataResult.id);
                list.Remove(driver);
                list.Add(driver);
            }
        }

        public override void Remove(Driver item)
        {
            HttpResponseMessage response = client.DeleteAsync(item.StaffId.ToString()).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                list.Remove(item);
            }
        }
    }
}
