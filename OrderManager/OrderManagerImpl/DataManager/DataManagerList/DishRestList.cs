﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;
using System.Net.Http.Headers;

using Newtonsoft.Json;

namespace OrderManagerImpl.DataManager.DelegateList
{
    class DishRestList : DataManagerList<Dish>
    {
        private const string URL = "http://40180582.pythonanywhere.com/dishes/";

        private HttpClient client;
        public DishRestList()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.Timeout = TimeSpan.FromSeconds(5);

            RepopulateList();

        }

        private void RepopulateList()
        {
            HttpResponseMessage response = client.GetAsync("").Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataList = JsonConvert.DeserializeObject(result);
                foreach (dynamic data in dataList)
                {
                    Dish dish = new Dish((int)data.id, (string)data.description, (int)data.price, (bool)data.vegetarian);
                    list.Add(dish);
                }
            }
        }

        public override void Add(Dish item)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("description", item.Description));
            data.Add(new KeyValuePair<string, string>("price", item.Price.ToString()));
            data.Add(new KeyValuePair<string, string>("vegetarian", item.Vegetarian.ToString()));
            HttpResponseMessage response = client.PostAsync("", new FormUrlEncodedContent(data)).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataResult = JsonConvert.DeserializeObject(result);
                Dish dish = new Dish((int)dataResult.id, (string)dataResult.description, (int)dataResult.price, (bool)dataResult.vegetarian);
                list.Add(dish);
            }
        }

        public override void Update(Dish item)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("description", item.Description));
            data.Add(new KeyValuePair<string, string>("price", item.Price.ToString()));
            data.Add(new KeyValuePair<string, string>("vegetarian", item.Vegetarian.ToString()));
            HttpResponseMessage response = client.PostAsync(item.Id.ToString() + "/", new FormUrlEncodedContent(data)).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataResult = JsonConvert.DeserializeObject(result);
                Dish dish = new Dish((int)dataResult.id, (string)dataResult.description, (int)dataResult.price, (bool)dataResult.vegetarian);
                list.Remove(dish);
                list.Add(dish);
            }
        }

        public override void Remove(Dish item)
        {
            HttpResponseMessage response = client.DeleteAsync(item.Id.ToString()).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                list.Remove(item);
            }
        }
    }
}
