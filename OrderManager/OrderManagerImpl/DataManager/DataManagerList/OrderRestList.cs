﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;
using System.Net.Http.Headers;

using Newtonsoft.Json;

namespace OrderManagerImpl.DataManager.DelegateList
{
    class OrderRestList : DataManagerList<Order>
    {
        private const string SIT_IN_URL = "http://40180582.pythonanywhere.com/sitin/";
        private const string DELIVERY_URL = "http://40180582.pythonanywhere.com/delivery/";

        HttpClient sitInClient;
        HttpClient deliveryClient;
        DataManager dataManager;
        public OrderRestList(DataManager dataManager)
        {
            this.dataManager = dataManager;
            sitInClient = new HttpClient();
            sitInClient.BaseAddress = new Uri(SIT_IN_URL);
            sitInClient.Timeout = TimeSpan.FromSeconds(5);
            deliveryClient = new HttpClient();
            deliveryClient.BaseAddress = new Uri(DELIVERY_URL);
            deliveryClient.Timeout = TimeSpan.FromSeconds(5);

            RepopulateList();

        }

        private void RepopulateList()
        {
            //Sit in
            HttpResponseMessage response = sitInClient.GetAsync("").Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataList = JsonConvert.DeserializeObject(result);
                foreach (dynamic data in dataList)
                {
                    SitInOrder order = new SitInOrder();
                    order.Table = (int)data.table;
                    order.Server = dataManager.Servers.Get((int)data.server);
                    foreach(string num in ((string)data.dishes).Split(','))
                    {
                        Dish dish = dataManager.Dishes.Get(int.Parse(num));
                        order.Dishes.Add(dish);
                    }
                    list.Add(order);
                }
            }
            //Delivery
            response = deliveryClient.GetAsync("").Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataList = JsonConvert.DeserializeObject(result);
                foreach (dynamic data in dataList)
                {
                    DeliveryOrder order = new DeliveryOrder();
                    order.CustomerName = (string)data.customerName;
                    order.DeliveryAddress = (string)data.deliveryAddress;
                    order.Driver = dataManager.Drivers.Get((int)data.driver); ;
                    foreach (string num in ((string)data.dishes).Split(','))
                    {
                        Dish dish = dataManager.Dishes.Get(int.Parse(num));
                        order.Dishes.Add(dish);
                    }
                    list.Add(order);
                }
            }

        }

        public override void Add(Order item)
        {
            genericPost(item, "");
        }

        private void genericPost(Order item, String url)
        {
            string dishes = String.Join(",", dataManager.Dishes.Select(x => x.Id));
            //Sitin
            if (item.GetType().Equals(typeof(SitInOrder)))
            {
                SitInOrder sitInOrder = (SitInOrder)item;
                var data = new List<KeyValuePair<string, string>>();
                data.Add(new KeyValuePair<string, string>("table", sitInOrder.Table.ToString()));
                data.Add(new KeyValuePair<string, string>("server", sitInOrder.Server.StaffId.ToString()));
                data.Add(new KeyValuePair<string, string>("dishes", dishes));
                HttpResponseMessage response = sitInClient.PostAsync(url, new FormUrlEncodedContent(data)).Result;
                Console.WriteLine(response.StatusCode);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    dynamic dataResult = JsonConvert.DeserializeObject(result);
                    SitInOrder order = new SitInOrder();
                    order.Table = (int)dataResult.table;
                    order.Server = dataManager.Servers.Get((int)dataResult.server);
                    foreach (string num in ((string)dataResult.dishes).Split(','))
                    {
                        Dish dish = dataManager.Dishes.Get(int.Parse(num));
                        order.Dishes.Add(dish);
                    }
                    list.Add(order);
                }
            }
            else
            {
                DeliveryOrder deliveryOrder = (DeliveryOrder)item;
                var data = new List<KeyValuePair<string, string>>();
                data.Add(new KeyValuePair<string, string>("customerName", deliveryOrder.CustomerName));
                data.Add(new KeyValuePair<string, string>("deliveryAddress", deliveryOrder.DeliveryAddress));
                data.Add(new KeyValuePair<string, string>("driver", deliveryOrder.Driver.StaffId.ToString()));
                data.Add(new KeyValuePair<string, string>("dishes", dishes));
                Console.WriteLine(deliveryOrder.CustomerName + "\n" + deliveryOrder.DeliveryAddress + "\n" + deliveryOrder.Driver.StaffId.ToString() + "\n" + dishes);
                HttpResponseMessage response = deliveryClient.PostAsync(url, new FormUrlEncodedContent(data)).Result;
                Console.WriteLine(response.StatusCode);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    dynamic dataResult = JsonConvert.DeserializeObject(result);
                    DeliveryOrder order = new DeliveryOrder();
                    order.CustomerName = (string)dataResult.customerName;
                    order.DeliveryAddress = (string)dataResult.deliveryAddress;
                    order.Driver = dataManager.Drivers.Get((int)dataResult.driver);
                    foreach (string num in ((string)dataResult.dishes).Split(','))
                    {
                        Dish dish = dataManager.Dishes.Get(int.Parse(num));
                        order.Dishes.Add(dish);
                    }
                    list.Add(order);
                }
            }
        }

        public override void Update(Order item)
        {
            genericPost(item, item.Id.ToString() + "/");
        }

        public override void Remove(Order item)
        {
            HttpResponseMessage response;
            if (item.GetType().Equals(typeof(SitInOrder)))
                response = sitInClient.DeleteAsync(item.Id.ToString()).Result;
            else
                response = deliveryClient.DeleteAsync(item.Id.ToString()).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                list.Remove(item);
            }
        }
    }
}
