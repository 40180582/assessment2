﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl.DataManager.DelegateList
{
    public abstract class DataManagerList<T> : IEnumerable<T> where T : KeyItem
    {
        protected List<T> list = new List<T>();
        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }


        public abstract void Add(T item);
        public abstract void Update(T item);
        public abstract void Remove(T item);

        public T Find(Predicate<T> match)
        {
            return list.Find(match);
        }

        public T Get(string s)
        {
            return list.Find(x => x.ToString().Equals(s));
        }

        public T Get(int key)
        {
            return list.Find(x => x.getKey() == key);
        }

        public int Count()
        {
            return list.Count;
        }
    }
}
