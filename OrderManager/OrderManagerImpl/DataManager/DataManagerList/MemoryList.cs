﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl.DataManager.DelegateList
{
    class MemoryList<T> : DataManagerList<T> where T : KeyItem
    {
        public override void Add(T item)
        {
            T found = list.Find(x => x.getKey().Equals(item.getKey()));
            list.Remove(found);
            list.Add(item);
        }

        public override void Update(T item)
        {
            T found = list.Find(x => x.getKey().Equals(item.getKey()));
            if (found == null)
                throw new ArgumentException("Does not exist in list");
            list.Remove(found);
            list.Add(item);
        }

        public override void Remove(T item)
        {
            Remove(item);
        }

        public override string ToString()
        {
            return String.Join(",", list.AsEnumerable());
        }
    }
}
