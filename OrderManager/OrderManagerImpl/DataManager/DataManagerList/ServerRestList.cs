﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;
using System.Net.Http.Headers;

using Newtonsoft.Json;

namespace OrderManagerImpl.DataManager.DelegateList
{
    class ServerRestList : DataManagerList<Server>
    {
        private const string URL = "http://40180582.pythonanywhere.com/server/";

        HttpClient client;
        public ServerRestList()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.Timeout = TimeSpan.FromSeconds(5);

            RepopulateList();

        }

        private void RepopulateList()
        {
            HttpResponseMessage response = client.GetAsync("").Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataList = JsonConvert.DeserializeObject(result);
                foreach (dynamic data in dataList)
                {
                    Server server = new Server((string)data.name, (int)data.id);
                    list.Add(server);
                }
            }
        }

        public override void Add(Server item)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("name", item.Name));
            HttpResponseMessage response = client.PostAsync("", new FormUrlEncodedContent(data)).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataResult = JsonConvert.DeserializeObject(result);
                Server server = new Server((string)dataResult.name, (int)dataResult.id);
                list.Add(server);
            }
        }

        public override void Update(Server item)
        {
            var data = new List<KeyValuePair<string, string>>();
            data.Add(new KeyValuePair<string, string>("name", item.Name));
            HttpResponseMessage response = client.PostAsync(item.StaffId.ToString() + "/", new FormUrlEncodedContent(data)).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                dynamic dataResult = JsonConvert.DeserializeObject(result);
                Server server = new Server((string)dataResult.name, (int)dataResult.id);
                list.Remove(server);
                list.Add(server);
            }
        }

        public override void Remove(Server item)
        {
            HttpResponseMessage response = client.DeleteAsync(item.StaffId.ToString()).Result;
            Console.WriteLine(response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                list.Remove(item);
            }
        }
    }
}
