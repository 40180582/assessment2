﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OrderManagerImpl.DataManager.DelegateList;

namespace OrderManagerImpl.DataManager
{
    public interface DataManager
    {
        DataManagerList<Dish> Dishes { get; }
        DataManagerList<Server> Servers { get; }
        DataManagerList<Driver> Drivers { get; }
        DataManagerList<Order> Orders { get; }

        void init();

        /*
         * Used for testing purposes only
         */
        void generateFakeData();
    }
}
