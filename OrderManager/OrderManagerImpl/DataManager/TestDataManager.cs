﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OrderManagerImpl.DataManager.DelegateList;

namespace OrderManagerImpl.DataManager
{
    public class TestDataManager : DataManager
    {
        /*
         * Must return 
         */
        private DataManagerList<Dish> dishes;
        public DataManagerList<Dish> Dishes { get { return dishes; } }

        private DataManagerList<Server> servers;
        public DataManagerList<Server> Servers { get { return servers; } }

        private DataManagerList<Driver> drivers;
        public DataManagerList<Driver> Drivers { get { return drivers; } }

        private DataManagerList<Order> orders;
        public DataManagerList<Order> Orders { get { return orders; } }


        /*
         * Used for testing purposes only
         */
        public void generateFakeData()
        {
            Servers.Add(new Server("Carol", 1));
            Servers.Add(new Server("Dave", 2));
            Servers.Add(new Server("Bob", 3));

            Drivers.Add(new Driver("Harry", "G123 JKL", 4));
            Drivers.Add(new Driver("Jane", "H456 ASD", 5));

            Dishes.Add(new Dish("Pasta", 495, true));
            Dishes.Add(new Dish("Curry", 600, false));
            Dishes.Add(new Dish("Risotto", 895, true));


            Console.WriteLine(Dishes.Get(1));
            Order o1 = new DeliveryOrder("Bob", "1 Fake Street", Drivers.Get(4));
            Console.WriteLine(o1 == null);
            o1.Dishes.Add(Dishes.Get(0));
            Orders.Add(o1);

            Server s = Servers.Get(2);
            Console.WriteLine(s == null);
            Order o2 = new SitInOrder(2, Servers.Get(2));
            o2.Dishes.Add(Dishes.Get(1));
            Orders.Add(o2);

            foreach (Dish d in Dishes)
                Console.WriteLine(d.Id);
            Console.WriteLine(Dishes);
            Order o3 = new DeliveryOrder("James", "1 False Lane", Drivers.Get(5));
            o3.Dishes.Add(Dishes.Get(0));
            o3.Dishes.Add(Dishes.Get(1));
            o3.Dishes.Add(Dishes.Get(2));
            Orders.Add(o3);

            // TODO Orders
        }


        public void init()
        {
            dishes = new MemoryList<Dish>();
            servers = new MemoryList<Server>();
            drivers = new MemoryList<Driver>();
            orders = new MemoryList<Order>();
        }
    }
}
