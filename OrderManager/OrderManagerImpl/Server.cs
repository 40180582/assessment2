﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    public class Server : Employee
    {

        public Server(String name) : base(name)
        {

        }

        public Server(String name, int staffId) : base(name, staffId)
        {

        }

        public override string ToString()
        {
            return String.Format("({0}) {1}", StaffId, Name);
        }

        public override int getKey()
        {
            return this.StaffId;
        }
    }
}
