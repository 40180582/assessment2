﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    public abstract class Employee : KeyItem
    {
        private String name;
        private static IdFactory ID_FACTORY = new IdFactory();

        public String Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("Can not be null or white space.");
                this.name = value;
            }

        }

        private int staffId;
        public int StaffId { get { return staffId; } }

        protected Employee(String name)
        {
            Name = name;
            staffId = ID_FACTORY.GetNextId();
        }

        protected Employee(String name, int staffId)
        {
            Name = name;
            this.staffId = staffId;
            ID_FACTORY.Add(staffId);
        }

        public abstract int getKey();
    }
}
