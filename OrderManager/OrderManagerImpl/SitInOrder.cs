﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl 
{
    public class SitInOrder : Order
    {
        private int table;
        public int Table {
            get { return table; }
            set
            {
                if (value < 0 || value > 10)
                    throw new ArgumentOutOfRangeException("Must be in the range 0 - 10");
                table = value;
            }
        }

        private Server server;
        public Server Server {
            get { return server; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                server = value;
            }
        }

        public SitInOrder() { }

        public SitInOrder(int table, Server server)
        {
            this.Table = table;
            this.Server = server;
        }

        public override string ToString()
        {
            return String.Format("Sit in: Table - {0}, Server - {1}, cost - {2:C2}", Table, Server, getCost()/100);
        }
    }
}
