﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    public class DeliveryOrder : Order
    {
        private String customerName;
        public String CustomerName {
            get { return customerName; }
            set
            {
                if(String.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("This can not be blank");
                customerName = value;
            }
        }

        private String deliveryAddress;
        public String DeliveryAddress {
            get { return deliveryAddress; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("This can not be blank");
                deliveryAddress = value;
            }
        }

        private Driver driver;
        public Driver Driver {
            get { return driver; }
            set
            {
                if (value==null)
                    throw new ArgumentNullException();
                driver = value;
            }
        }

        public DeliveryOrder() { }

        public DeliveryOrder(String customerName, String customerAddress, Driver driver)
        {
            this.customerName = customerName;
            this.deliveryAddress = customerAddress;
            this.driver = driver;
        }

        public override string ToString()
        {
            return String.Format("Delivery: Name - {0}, Address - {1}, Driver - {2}, Cost - {3:C2}", CustomerName, DeliveryAddress, Driver, getCost()/100);
        }
    }
}
