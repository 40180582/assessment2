﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    public class Driver : Employee
    {
        public String CarRegistration { get; set; }

        public Driver(String name, String carRegistration) : base(name)
        {
            CarRegistration = carRegistration;
        }

        public Driver(String name, String carRegistration, int staffId) : base(name, staffId)
        {
            CarRegistration = carRegistration;
        }

        public override string ToString()
        {
            return String.Format("({0}) {1}", StaffId, Name);
        }
        public override int getKey()
        {
            return this.StaffId;
        }
    }
}
