﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManagerImpl
{
    /*
     * Used to create unique incremental ids
     */
    public class IdFactory
    {
        private List<int> usedIds = new List<int>();

        /// <returns>The next available staff id</returns>
        public int GetNextId()
        {
            for (int i = 1; i < Int32.MaxValue; i++)
            {
                if (usedIds.Contains(i))
                    continue;
                usedIds.Add(i);
                return i;
            }
            throw new IndexOutOfRangeException("Too many items created.!");
        }

        /// <summary>
        /// Adds an id to the used list of specifically defined instead of using GetNextStaffId
        /// </summary>
        /// <param name="id">The id to be added</param>
        public void Add(int id)
        {
            if (usedIds.Contains(id))
                throw new ArgumentException("This is has already been used.");
            usedIds.Add(id);
        }

    }
}
