﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for Sales.xaml
    /// </summary>
    public partial class SalesWindow : Window
    {
        private DataManager datamanager;
        public SalesWindow(DataManager datamanager)
        {
            this.datamanager = datamanager;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Dictionary<Dish, int> counts = new Dictionary<Dish, int>();
            foreach(Dish dish in datamanager.Dishes)
            {
                counts.Add(dish, 0);
            }
            foreach(Order order in datamanager.Orders)
            {
                foreach(Dish dish in order.Dishes)
                {
                    counts[dish] = counts[dish] + 1;
                }
            }
            foreach(KeyValuePair<Dish, int> count in counts)
            {
                LstSales.Items.Add(String.Format("{0}.   Count - {1}", count.Key.ToString(), count.Value));
            }
        }
    }
}
