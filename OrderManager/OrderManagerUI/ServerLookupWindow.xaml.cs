﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for ServerLookup.xaml
    /// </summary>
    public partial class ServerLookupWindow : Window
    {
        private DataManager dataManager;
        public ServerLookupWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach(Server server in dataManager.Servers)
            {
                DrpServers.Items.Add(server.ToString());
            }
            DrpServers.SelectedIndex = 0;
        }

        private void DrpServers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LstOrders.Items.Clear();
            Server server = dataManager.Servers.Find(x => x.ToString().Equals(DrpServers.SelectedItem));
            foreach(Order order in dataManager.Orders)
            {
                if(!order.GetType().Equals(typeof(SitInOrder)))
                    continue;
                SitInOrder sio = (SitInOrder)order;
                if(sio.Server==server)
                {
                    ListBoxItem lbi = new ListBoxItem();
                    lbi.Content = sio.ToString();
                    lbi.MouseDoubleClick += ViewDishes;
                    LstOrders.Items.Add(lbi);
                }
            }
        }

        private void ViewDishes(object sender, RoutedEventArgs e)
        {
            if (!sender.GetType().Equals(typeof(ListBoxItem)))
                return;
            LstDishes.Items.Clear();
            ListBoxItem lbi = (ListBoxItem)sender;
            foreach (Order order in dataManager.Orders)
            {
                if (lbi.Content.Equals(order.ToString()))
                {
                    foreach (Dish dish in order.Dishes)
                    {
                        LstDishes.Items.Add(dish.ToString());
                    }
                }
            }
        }
    }
}
