﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Globalization;

namespace OrderManager
{
    class Validators
    {
        public static String Validate_Name(TextBox textBox, bool error, String message)
        {
            String name = textBox.Text;
            if (String.IsNullOrWhiteSpace(name))
            {
                if (error)
                {
                    MessageBox.Show(message);
                }
                return null;
            }
            return name;
        }

        public static int? ValidateStaffId(TextBox textBox, bool error, String message)
        {
            try
            {
                int id = Int32.Parse(textBox.Text);
                return id;
            }
            catch (System.Exception)
            {
                if (error)
                    MessageBox.Show(message);
                return null;
            }
        }

        public static int? ValidatePrice(TextBox textBox, bool error, String message)
        {
            try
            {
                decimal price = decimal.Parse(textBox.Text, NumberStyles.Currency);
                return (int)(price*100);
            }
            catch (System.Exception)
            {
                if (error)
                    MessageBox.Show(message);
                return null;
            }
        }
    }
}
