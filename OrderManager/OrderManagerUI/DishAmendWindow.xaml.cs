﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for DishAmendWindow.xaml
    /// </summary>
    public partial class DishAmendWindow : Window
    {
        private DataManager dataManager;
        private Dish current;
        public DishAmendWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DrpDishes.Items.Clear();
            foreach(Dish dish in dataManager.Dishes)
            {
                DrpDishes.Items.Add(dish.ToString());
            }
            if (sender == Window)
                DrpDishes.SelectedIndex = 0;
            else
                DrpDishes.SelectedIndex = DrpDishes.Items.Count - 1;
        }

        private void DrpServers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DrpDishes.SelectedItem == null)
                return;
            current = dataManager.Dishes.Find(x => x.ToString().Equals(DrpDishes.SelectedItem));
            TxtDescription.Text = current.Description;
            TxtPrice.Text = String.Format("{0:C2}", current.Price/100.0);
            ChckVegetarian.IsChecked = current.Vegetarian;
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            int? price = Validators.ValidatePrice(TxtPrice, true, "Please enter a valid price.");
            String description = Validators.Validate_Name(TxtDescription, true, "Description can not be blank.");
            if (price == null || description == null)
                return;
            Dish dish = new Dish(description, (int)price, (bool)ChckVegetarian.IsChecked);
            dataManager.Dishes.Add(dish);
            Window_Loaded(null, null);
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            int? price = Validators.ValidatePrice(TxtPrice, true, "Please enter a valid price.");
            String description = Validators.Validate_Name(TxtDescription, true, "Description can not be blank.");
            if (price == null || description == null)
                return;
            current.Price = (int)price;
            current.Description = description;
            current.Vegetarian = (bool)ChckVegetarian.IsChecked;
            dataManager.Dishes.Update(current);
            Window_Loaded(null, null);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (current == null)
                return;
            dataManager.Dishes.Remove(current);
            DrpDishes.Items.Remove(current);
            Window_Loaded(null, null);
        }
    }
}
