﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for ManagerOrderWindow.xaml
    /// </summary>
    public partial class ManagerOrderWindow : Window
    {
        private DataManager dataManager;
        public ManagerOrderWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
            PopulateLstOrders();
        }

        private void PopulateLstOrders()
        {
            bool sitIn = (bool)ChckSitIn.IsChecked;
            bool delivery = (bool)ChckDelivery.IsChecked;
            LstOrders.Items.Clear();
            foreach(Order order in dataManager.Orders)
            {
                ListBoxItem li = new ListBoxItem();
                if(order.GetType().Equals(typeof(SitInOrder)) && sitIn)
                {
                    li.Content = ((SitInOrder)order).ToString();
                }
                if (order.GetType().Equals(typeof(DeliveryOrder)) && delivery)
                {
                    li.Content = ((DeliveryOrder)order).ToString();
                }
                li.MouseDoubleClick += ViewDishes;
                LstOrders.Items.Add(li);
            }
        }

        private void ChckSitIn_Click(object sender, RoutedEventArgs e)
        {
            PopulateLstOrders();
        }

        private void ChckDelivery_Click(object sender, RoutedEventArgs e)
        {
            PopulateLstOrders();
        }

        private void ViewDishes(object sender, RoutedEventArgs e)
        {
            if (!sender.GetType().Equals(typeof(ListBoxItem)))
                return;
            LstDishes.Items.Clear();
            ListBoxItem lbi = (ListBoxItem)sender;
            foreach(Order order in dataManager.Orders)
            {
                if(lbi.Content.Equals(order.ToString()))
                {
                    foreach(Dish dish in order.Dishes)
                    {
                        LstDishes.Items.Add(dish.ToString());
                    }
                }
            }
        }
    }
}
