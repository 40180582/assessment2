﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataManager dataManager;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WdwMain_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                dataManager = new RestDataManager();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                MessageBox.Show("Unable to connect to web server.\nReverting to local storage.\nChanges will not be saved.");
                dataManager = new TestDataManager();
            }
            dataManager.init();
        }

        private void BtnNewOrder_Click(object sender, RoutedEventArgs e)
        {
            NewOrderWindow orderWindow = new NewOrderWindow(dataManager);
            orderWindow.Show();
        }

        private void BtnManager_Click(object sender, RoutedEventArgs e)
        {
            ManagerWindow managerWindow = new ManagerWindow(dataManager);
            managerWindow.Show();
        }
    }
}
