﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for DriverAmendWindow.xaml
    /// </summary>
    public partial class DriverAmendWindow : Window
    {
        private DataManager dataManager;
        private Driver current;
        public DriverAmendWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DrpDrivers.Items.Clear();
            foreach(Driver driver in dataManager.Drivers)
            {
                DrpDrivers.Items.Add(driver.ToString());
            }
            if (sender == Window)
                DrpDrivers.SelectedIndex = 0;
            else
                DrpDrivers.SelectedIndex = DrpDrivers.Items.Count - 1;
        }

        private void DrpServers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DrpDrivers.SelectedItem == null)
                return;
            current = dataManager.Drivers.Find(x => x.ToString().Equals(DrpDrivers.SelectedItem));
            TxtName.Text = current.Name;
            TxtRegistration.Text = current.CarRegistration;
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            String name = Validators.Validate_Name(TxtName, true, "Name can not be blank.");
            String reg = Validators.Validate_Name(TxtRegistration, true, "Registration can not be blank.");
            if (name == null || reg == null)
                return;
            Driver driver = new Driver(name, reg);
            dataManager.Drivers.Add(driver);
            Window_Loaded(null, null);
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            String name = Validators.Validate_Name(TxtName, true, "Name can not be blank.");
            String reg = Validators.Validate_Name(TxtRegistration, true, "Registration can not be blank.");
            if (name == null || reg == null)
                return;
            current.Name = name;
            current.CarRegistration = reg;
            dataManager.Drivers.Update(current);
            Window_Loaded(null, null);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (current == null)
                return;
            dataManager.Drivers.Remove(current);
            Window_Loaded(null, null);
        }
    }
}
