﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for BillWindow.xaml
    /// </summary>
    public partial class BillWindow : Window
    {
        public BillWindow(Order order)
        {
            InitializeComponent();
            int total = 0;
            foreach(Dish dish in order.Dishes)
            {
                Label l = new Label();
                l.Content = dish.ToString();
                StckOrderSummary.Children.Add(l);
                StckOrderSummary.Height += l.Height;
                total += dish.Price;
            }
            LblTotal.Content = String.Format("Total: {0:C2}", total/100.0);
            if(order.GetType().Equals(typeof(SitInOrder)))
            {
                SitInOrder sio = (SitInOrder)order;
                GrdSitIn.Visibility = Visibility.Visible;
                lblServer.Content = "Server: " + sio.Server.ToString();
                LblTable.Content = "Table: " + sio.Table.ToString();
            }
            else
            {
                DeliveryOrder delo = (DeliveryOrder)order;
                GrdDelivery.Visibility = Visibility.Visible;
                LblDriver.Content = "Driver: " + delo.Driver.ToString();
                LblName.Content = "Name: " + delo.CustomerName.ToString();
                LblAddress.Content = "Address: " + delo.DeliveryAddress.ToString();
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
