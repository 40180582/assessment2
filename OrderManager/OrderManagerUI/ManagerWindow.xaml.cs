﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for Manager.xaml
    /// </summary>
    public partial class ManagerWindow : Window
    {
        private DataManager dataManager;
        public ManagerWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
        }

        private void BtnServers_Click(object sender, RoutedEventArgs e)
        {
            ServerAmendWindow window = new ServerAmendWindow(dataManager);
            window.Show();
        }

        private void BtnMenu_Click(object sender, RoutedEventArgs e)
        {
            DishAmendWindow window = new DishAmendWindow(dataManager);
            window.Show();
        }

        private void BtnDrivers_Click(object sender, RoutedEventArgs e)
        {
            DriverAmendWindow window = new DriverAmendWindow(dataManager);
            window.Show();
        }

        private void BtnOrderDetail_Click(object sender, RoutedEventArgs e)
        {
            ManagerOrderWindow window = new ManagerOrderWindow(dataManager);
            window.Show();
        }

        private void BtnSalesSummary_Click(object sender, RoutedEventArgs e)
        {
            SalesWindow window = new SalesWindow(dataManager);
            window.Show();
        }

        private void BtnServerLookup_Click(object sender, RoutedEventArgs e)
        {
            ServerLookupWindow window = new ServerLookupWindow(dataManager);
            window.Show();
        }
    }
}
