﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for ServerAmendWindow.xaml
    /// </summary>
    public partial class ServerAmendWindow : Window
    {
        private DataManager dataManager;
        private Server current;
        public ServerAmendWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DrpServers.Items.Clear();
            foreach (Server server in dataManager.Servers)
            {
                DrpServers.Items.Add(server.ToString());
            }
            if (sender == Window)
                DrpServers.SelectedIndex = 0;
            else
                DrpServers.SelectedIndex = DrpServers.Items.Count - 1;
        }

        private void DrpServers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DrpServers.SelectedItem == null)
                return;
            current = dataManager.Servers.Find(x => x.ToString().Equals(DrpServers.SelectedItem));
            TxtName.Text = current.Name;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (current == null)
                return;
            dataManager.Servers.Remove(current);
            DrpServers.Items.Remove(current);
            Window_Loaded(null, null);
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            String name = Validators.Validate_Name(TxtName, true, "Name can not be blank.");
            if (name == null)
                return;
            current.Name = name;
            dataManager.Servers.Update(current);
            Window_Loaded(null, null);
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            String name = Validators.Validate_Name(TxtName, true, "Name can not be blank.");
            if (name == null)
                return;
            Server server = new Server(name);
            dataManager.Servers.Add(server);
            Window_Loaded(null, null);
        }
    }
}
