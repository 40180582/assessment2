﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManager
{
    /// <summary>
    /// Interaction logic for SitInOrder.xaml
    /// </summary>
    public partial class NewOrderWindow : Window
    {
        private delegate Boolean Validator(bool display);

        private DataManager dataManager;
        private Boolean loaded;
        private Brush defaultBorder, errorBorder, successBorder;
        private Order testOrder;
        private Dictionary<String, Validator> validators;
        private Dictionary<String, bool> changedFlags;
        public NewOrderWindow(DataManager dataManager)
        {
            this.dataManager = dataManager;
            InitializeComponent();
            errorBorder = new SolidColorBrush(Colors.Red);
            successBorder = new SolidColorBrush(Colors.Green);
            validators = new Dictionary<String, Validator>();
            changedFlags = new Dictionary<String, bool>();
            loaded = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Populate server list
            foreach(Server server in dataManager.Servers)
            {
                DrpServer.Items.Add(server);
            }
            DrpServer.SelectedIndex = 0; // Remove blank default
            // Populate driver list
            foreach (Driver driver in dataManager.Drivers)
            {
                DrpDriver.Items.Add(driver);
            }
            DrpDriver.SelectedIndex = 0; // Remove blank default
            foreach(Dish dish in dataManager.Dishes)
            {
                Button b = new Button();
                b.Content = dish.ToString();
                b.Click += AddItem_Click;
                b.FontSize = 14;
                StckMenuSelect.Children.Add(b);
                StckMenuSelect.Height += b.Height;
            }
            // Set normal and error border brushes
            defaultBorder = TxtTableNo.BorderBrush;

            //Set validators
            validators.Add(TxtTableNo.Name, TxtTableNo_Validate);
            validators.Add(TxtName.Name, TxtName_Validate);
            validators.Add(TxtAddress.Name, TxtAddress_Validate);

            //Set changed flags
            changedFlags.Add(TxtTableNo.Name, false);
            changedFlags.Add(TxtName.Name, false);
            changedFlags.Add(TxtAddress.Name, false);
        }

        /// <summary>
        /// Adds clicked item into LstDishes
        /// </summary>
        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            if(!sender.GetType().Equals(typeof(Button)))
                return;
            Button b = (Button)sender;
            foreach(Dish d in dataManager.Dishes)
            {
                if(d.ToString().Equals(b.Content))
                {
                    ListBoxItem lbi = new ListBoxItem();
                    lbi.Content = b.Content;
                    lbi.MouseDoubleClick += RemoveItem_Click;
                    LstDishes.Items.Add(lbi);
                    return;
                }
            }
        }

        /// <summary>
        /// Removes double clicked item from LstDishes
        /// </summary>
        private void RemoveItem_Click(object sender, RoutedEventArgs e)
        {
            if (!sender.GetType().Equals(typeof(ListBoxItem)))
                return;
            ListBoxItem lbi = (ListBoxItem)sender;
            LstDishes.Items.Remove(lbi);
        }

        /// <summary>
        /// Customises the UI depending on selected delivery method
        /// </summary>
        private void RdioSitInDelivery_Checked(object sender, RoutedEventArgs e)
        {
            if(sender == RdioSitIn)
                testOrder = new SitInOrder();
            else
                testOrder = new DeliveryOrder();
            if (!loaded)
                return;
            int offset = (sender == RdioSitIn) ? 72 : -72;
            // Offset items below check boxes to allow space for net textboxes
            ScrlMenuSelect.Height += offset;
            Thickness margin = ScrlMenuSelect.Margin;
            margin.Top -= offset;
            ScrlMenuSelect.Margin = margin;
            // Set visibility
            if(sender == RdioSitIn)
            {
                GrdSitInDetails.Visibility = Visibility.Visible;
                GrdDeliveryDetails.Visibility = Visibility.Hidden;
            }
            else
            {
                GrdDeliveryDetails.Visibility = Visibility.Visible;
                GrdSitInDetails.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Used to call validators on all text boxes
        /// </summary>
        private void All_LostFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                String name = ((TextBox)sender).Name;
                if(changedFlags[name])
                    validators[name](true);
            }
        }

        /// <summary>
        /// Used to set changedFlags for textboxes
        /// </summary>
        private void All_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                changedFlags[((TextBox)sender).Name] = true;
            }
        }

        /// <summary>
        /// Validates TxtTableNo
        /// </summary>
        /// <param name="display">Display error using MessageBox.Show()</param>
        /// <returns>True on valid input</returns>
        private bool TxtTableNo_Validate(bool display)
        {
            if (!(testOrder is SitInOrder)) // Check if SitInOrder is selected
                return true;
            try
            {
                SitInOrder order = (SitInOrder)testOrder;
                String txt = TxtTableNo.Text;
                int value = Int32.Parse(txt);
                order.Table = value;
                TxtTableNo.BorderBrush = successBorder;
                return true;
            }
            catch (Exception ex)
            {
                TxtTableNo.BorderBrush = errorBorder;
                if (display)
                {
                    if (ex is ArgumentOutOfRangeException)
                        MessageBox.Show("Table: " + ex.Message);
                    else
                        MessageBox.Show("Table: Please enter a valid number.");
                }
                return false;
            }
        }

        private bool TxtName_Validate(bool display)
        {
            if (!(testOrder is DeliveryOrder)) // Check if DeliveryOrder is selected
                return true;
            try
            {
                DeliveryOrder order = (DeliveryOrder)testOrder;
                String txt = TxtName.Text;
                order.CustomerName = txt;
                TxtName.BorderBrush = successBorder;
                return true;
            }
            catch (Exception ex)
            {
                TxtName.BorderBrush = errorBorder;
                if(display)
                {
                    MessageBox.Show("Name: " + ex.Message);
                }
                return false;
            }
        }

        private bool TxtAddress_Validate(bool display)
        {
            if (!(testOrder is DeliveryOrder)) // Check if DeliveryOrder is selected
                return true;
            try
            {
                DeliveryOrder order = (DeliveryOrder)testOrder;
                String txt = TxtAddress.Text;
                order.DeliveryAddress = txt;
                TxtAddress.BorderBrush = successBorder;
                return true;
            }
            catch (Exception ex)
            {
                TxtAddress.BorderBrush = errorBorder;
                if (display)
                {
                    MessageBox.Show("Address: " + ex.Message);
                }
                return true;
            }
        }

        private void BtnBill_Click(object sender, RoutedEventArgs e)
        {
            bool flag = true;
            foreach(Validator v in validators.Values)
            {
                if (!v(false)) // Check all valid
                    flag = false;
            }
            if (!flag) // Allow checking all fields
                return;
            foreach(ListBoxItem item in LstDishes.Items)
            {
                Console.WriteLine(item.Content);
                Dish d = this.dataManager.Dishes.Find(x => x.ToString().Equals((String)item.Content));
                this.testOrder.Dishes.Add(d);
            }
            if(this.testOrder.GetType().Equals(typeof(SitInOrder)))
            {
                Server s = dataManager.Servers.Find(x => x.ToString().Equals(DrpServer.SelectedItem.ToString()));
                if (s == null)
                    return;
                SitInOrder sio = (SitInOrder)testOrder;
                sio.Server = s;
            }
            else
            {
                Driver d = dataManager.Drivers.Find(x => x.ToString().Equals(DrpDriver.SelectedItem.ToString()));
                if (d == null)
                    return;
                DeliveryOrder delo = (DeliveryOrder)testOrder;
                delo.Driver = d;
            }
            BillWindow bw = new BillWindow(this.testOrder);
            dataManager.Orders.Add(this.testOrder);
            bw.Show();
        }
    }
}
