﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using OrderManagerImpl;

namespace OrderManagerTest
{
    /// <summary>
    /// Summary description for EmployeeTest
    /// </summary>
    [TestClass]
    public class EmployeeTest
    {

        [TestMethod]
        public void TestStaffId()
        {
            Employee s1 = new Server("s1");
            Assert.AreEqual(s1.StaffId, 1);
            Employee d2 = new Driver("d2", "r1");
            Assert.AreEqual(d2.StaffId, 2);
            Employee s3 = new Server("s3", 3);
            Employee s4 = new Server("s4");
            Assert.AreEqual(s4.StaffId, 4);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestEmptyName()
        {
            Employee e = new Server("");
        }
    }
}
