﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using OrderManagerImpl;
using OrderManagerImpl.DataManager;

namespace OrderManagerTest
{
    /// <summary>
    /// Summary description for MenuTest
    /// </summary>
    [TestClass]
    public class MenuTest
    {
        [TestMethod]
        public void DishAddUpdate()
        {
            DataManager dm = new TestDataManager();
            dm.init();
            int old = dm.Dishes.Count();
            Dish d = new Dish("d1", 1, true);
            Assert.AreEqual(dm.Dishes.Count(), old);
            dm.Dishes.Add(d);
            Assert.AreEqual(dm.Dishes.Count(), old+1);
            Assert.AreEqual(dm.Dishes.Get(d.Id).Price, 1);
            d.Price=2;
            dm.Dishes.Update(d);
            Assert.AreEqual(dm.Dishes.Get(d.Id).Price, 2);
        }

        [TestMethod]
        public void DriverAddUpdate()
        {
            DataManager dm = new TestDataManager();
            dm.init();
            int old = dm.Drivers.Count();
            Driver d = new Driver("d1", "r1");
            Assert.AreEqual(dm.Drivers.Count(), old);
            dm.Drivers.Add(d);
            Assert.AreEqual(dm.Drivers.Count(), old+1);
            Assert.AreEqual(dm.Drivers.Get(d.StaffId).Name, "d1");
            d.Name = "d2";
            dm.Drivers.Update(d);
            Assert.AreEqual(dm.Drivers.Get(d.StaffId).Name, "d2");
        }

        [TestMethod]
        public void ServerAddUpdate()
        {
            DataManager dm = new TestDataManager();
            dm.init();
            int old = dm.Servers.Count();
            Server s = new Server("s1");
            Assert.AreEqual(dm.Servers.Count(), old);
            dm.Servers.Add(s);
            Assert.AreEqual(dm.Servers.Count(), old+1);
            Assert.AreEqual(dm.Servers.Get(s.StaffId).Name, "s1");
            s.Name = "s2";
            dm.Servers.Update(s);
            Assert.AreEqual(dm.Servers.Get(s.StaffId).Name, "s2");
        }

        [TestMethod]
        public void SitInOrderAddUpdate()
        {
            DataManager dm = new TestDataManager();
            dm.init();
            int old = dm.Orders.Count();
            dm.Dishes.Add(new Dish("d1", 1, true));
            SitInOrder s = new SitInOrder();
            Assert.AreEqual(dm.Orders.Count(), old);
            dm.Orders.Add(s);
            Assert.AreEqual(dm.Orders.Count(), old+1);
            s.Dishes.Add(dm.Dishes.Get(0));
            dm.Orders.Update(s);
            Assert.AreEqual(dm.Orders.Get(s.Id).Dishes.Count, 1);
        }

        [TestMethod]
        public void DeliveryOrderAddUpdate()
        {
            DataManager dm = new TestDataManager();
            dm.init();
            int old = dm.Orders.Count();
            dm.Dishes.Add(new Dish("d1", 1, true));
            DeliveryOrder s = new DeliveryOrder();
            Assert.AreEqual(dm.Orders.Count(), old);
            dm.Orders.Add(s);
            Assert.AreEqual(dm.Orders.Count(), old + 1);
            s.Dishes.Add(dm.Dishes.Get(0));
            dm.Orders.Update(s);
            Assert.AreEqual(dm.Orders.Get(s.Id).Dishes.Count, 1);
        }
    }
}
