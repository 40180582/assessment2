from rest_framework import status

class ListGetMixin():
    model = None
    list_url = None

    def get_model(self):
        return self.model

    def get_list_url(self):
        return self.list_url

    def test_list_get(self):
        response = self.client.get(self.get_list_url(), {})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(self.get_model().objects.all()))

class UpdateMixin():
    model = None
    update_url = None
    update_data = None
    update_check_models = True

    def get_model(self):
        return self.model

    def get_update_url(self):
        return self.update_url

    def get_update_data(self):
        return self.update_data

    def test_update(self):
        count = len(self.get_model().objects.all())
        response = self.client.post(self.get_update_url(),
                                    self.get_update_data(),
                                   )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for (key, value) in self.get_update_data().items():
            self.assertEqual(response.data[key], value)
            if self.update_check_models:
                self.assertEqual(
                    getattr(self.get_model().objects.get(id=1), key),
                    value
                )
        self.assertEqual(len(self.get_model().objects.all()), count)

class DestroyMixin():
    model = None
    destroy_url = None
    destroy_pk = None

    def get_destroy_url(self):
        return self.destroy_url

    def get_destroy_pk(self):
        return self.destroy_pk

    def test_destroy(self):
        count = len(self.get_model().objects.all())
        response = self.client.delete(self.get_destroy_url())
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(self.get_model().objects.all()), count-1)
        self.assertRaises(self.get_model().DoesNotExist,
                          self.get_model().objects.get,
                          **self.get_destroy_pk()
                         )
