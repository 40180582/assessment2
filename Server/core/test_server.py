from rest_framework.test import APITestCase
from rest_framework import status

import models
import test_helper

class EmployeeTests(APITestCase,
                      test_helper.ListGetMixin,
                      test_helper.UpdateMixin,
                      test_helper.DestroyMixin):
    model = models.Server
    list_url = '/server/'
    update_url = '/server/1/'
    update_data = {'name': 'Name Updated'}
    destroy_url = '/server/1/'
    destroy_pk = {'id': 1}

    def setUp(self):
        self.get_model()(name='S1').save()
        self.get_model()(name='S2').save()
        self.get_model()(name='S3').save()

    def test_unicode(self):
        self.assertEquals(
            unicode(self.get_model().objects.get(id=1)),
            u'<Server: (1) Name=S1>'
        )
        self.assertEquals(
            unicode(self.get_model().objects.get(id=2)),
            u'<Server: (2) Name=S2>'
        )
        self.assertEquals(
            unicode(self.get_model().objects.get(id=3)),
            u'<Server: (3) Name=S3>'
        )

    def test_list_create(self):
        # Empty
        response = self.client.post('/server/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'name': [u'This field is required.']}
                        )

        # Blank fields
        response = self.client.post('/server/',
                                    {'name': ''}
                                   )
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['name'],
                         [u'This field may not be blank.']
                        )

        # Valid
        response = self.client.post('/server/',
                                    {'name': 'S4'}
                                   )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(self.get_model().objects.all()), 4)
