from rest_framework.test import APITestCase
from rest_framework import status

import models
import test_helper

class SitInOrderTests(APITestCase,
                      test_helper.ListGetMixin,
                      #test_helper.UpdateMixin,
                      test_helper.DestroyMixin):
    model = models.SitInOrder
    list_url = '/sitin/'
    update_url = '/sitin/1/'
    update_data = {'table': 10,
                   'dishes':'1,2'}
    update_check_models = False
    destroy_url = '/sitin/1/'
    destroy_pk = {'id': 1}

    def setUp(self):
        d1 = models.Dish(description='curry',
                    price=400,
                    vegetarian=False
                   )
        d1.save()
        d2 = models.Dish(description='chips',
                    price=300,
                    vegetarian=True
                   )
        d2.save()
        models.DishCount(dish=d1, count=2).save()
        models.DishCount(dish=d2, count=3).save()
        o1 = models.SitInOrder(table=1)
        o1.save()
        o2 = models.SitInOrder(table=2)
        o2.save()
        o2.dishes.add(models.DishCount.objects.get(id=1))
        o2.save()
        o3 = models.SitInOrder(table=3)
        o3.save()
        map(o3.dishes.add, models.DishCount.objects.all())
        o3.save()

    def test_unicode(self):
        self.assertEquals(
            unicode(self.get_model().objects.get(id=1)),
            u'<SitInOrder: Table=1 Dishes=[]>'
        )
        dish = models.Dish.objects.get(id=1)
        self.assertEquals(
            unicode(self.get_model().objects.get(id=2)),
            u'<SitInOrder: Table=2 Dishes=[{}]>'.format(dish)
        )
        dish = u','.join(map(unicode, models.Dish.objects.all()))
        self.assertEquals(
            unicode(self.get_model().objects.get(id=3)),
            u'<SitInOrder: Table=3 Dishes=[{}]>'.format(dish)
        )

    def test_list_create(self):
        # Empty
        response = self.client.post('/sitin/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'table': [u'This field is required.'],
                          'dishes': [u'This field is required.']},
                        )

        # Invalid table
        for table in [-1, 11]:
            response = self.client.post('/sitin/',
                                        {'table': table}
                                       )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data['table'],
                             [u'This field must be in the range 0-10.']
                            )
        #Invalid dishes
        for dish in [-1, 11]:
            response = self.client.post('/sitin/',
                                        {'dishes': dish}
                                       )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data['dishes'],
                             [u'This field must be a valid dish id.']
                            )

        response = self.client.post('/sitin/',
                                    {'table': 1,
                                     'dishes': '1,2,1'}
                                   )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(self.get_model().objects.all()), 4)
