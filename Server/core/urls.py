from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'^dishes/$', views.DishList.as_view(), name='dishes-list'),
    url(r'^dishes/(?P<pk>[0-9]+)/$', views.DishUpdate.as_view(),
        name='dishes-update'),
    url(r'^sitin/$', views.SitInOrderList.as_view(), name='sitin-list'),
    url(r'^sitin/(?P<pk>[0-9]+)/$', views.SitInOrderUpdate.as_view(), name='sitin-update'),
    url(r'^delivery/$', views.DeliveryOrderList.as_view(),
        name='delivery-list'),
    url(r'^delivery/(?P<pk>[0-9]+)/$', views.DeliveryOrderUpdate.as_view(),
        name='delivery-update'),
    url(r'^server/$', views.ServerList.as_view(),
        name='server-list'),
    url(r'^server/(?P<pk>[0-9]+)/$', views.ServerUpdate.as_view(),
        name='server-update'),
    url(r'^driver/$', views.DriverList.as_view(),
        name='driver-list'),
    url(r'^driver/(?P<pk>[0-9]+)/$', views.DriverUpdate.as_view(),
        name='driver-update'),
)
