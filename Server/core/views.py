from rest_framework import generics, mixins

import models
import serializers

class UpdateDestroyAPIView(generics.GenericAPIView,
                 mixins.UpdateModelMixin,
                 mixins.DestroyModelMixin):

    def delete(self, request, *args, **kwargs):
        return super(UpdateDestroyAPIView, self).destroy(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super(UpdateDestroyAPIView, self).partial_update(request, *args, **kwargs)

class DishList(generics.ListCreateAPIView):
    queryset = models.Dish.objects.all()
    serializer_class = serializers.DishSerializer

class DishUpdate(UpdateDestroyAPIView):
    queryset = models.Dish.objects.all()
    serializer_class = serializers.DishSerializer

class SitInOrderList(generics.ListCreateAPIView):
    queryset = models.SitInOrder.objects.all()
    serializer_class = serializers.SitInOrderSerializer

class SitInOrderUpdate(UpdateDestroyAPIView):
    queryset = models.SitInOrder.objects.all()
    serializer_class = serializers.SitInOrderSerializer

class DeliveryOrderList(generics.ListCreateAPIView):
    queryset = models.DeliveryOrder.objects.all()
    serializer_class = serializers.DeliveryOrderSerializer

class DeliveryOrderUpdate(UpdateDestroyAPIView):
    queryset = models.DeliveryOrder.objects.all()
    serializer_class = serializers.DeliveryOrderSerializer

class ServerList(generics.ListCreateAPIView):
    queryset = models.Server.objects.all()
    serializer_class = serializers.ServerSerializer

class ServerUpdate(UpdateDestroyAPIView):
    queryset = models.Server.objects.all()
    serializer_class = serializers.ServerSerializer

class DriverList(generics.ListCreateAPIView):
    queryset = models.Driver.objects.all()
    serializer_class = serializers.DriverSerializer

class DriverUpdate(UpdateDestroyAPIView):
    queryset = models.Driver.objects.all()
    serializer_class = serializers.DriverSerializer
