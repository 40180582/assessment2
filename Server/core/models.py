from django.db import models

import locale

class Dish(models.Model):
    description = models.CharField(max_length=30)
    price = models.IntegerField()
    vegetarian = models.BooleanField()
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return u'<Dish: {} {}{}>'.format(self.description,
                                  locale.currency(self.price/100.0, grouping=True).decode(locale.getlocale()[1]),
                                  u' (v)' if self.vegetarian else u'')

class DishCount(models.Model):
    dish = models.ForeignKey(Dish)
    count = models.IntegerField()

    #TODO
    def __unicode__(self):
        return ",".join([str(self.dish.id)]*self.count)

class Order(models.Model):
    dishes = models.ManyToManyField(DishCount)
    enabled = models.BooleanField(default=True)

class SitInOrder(Order):
    table = models.IntegerField()
    server = models.ForeignKey('Server')

    def __unicode__(self):
        #d = u'[{}]'.format(u','.join(map(unicode, self.dishes.all())))
        d = 'TODO'
        return u'<SitInOrder: Table={} Dishes={}>'.format(self.table, d)

class DeliveryOrder(Order):
    customerName = models.CharField(max_length=30)
    deliveryAddress = models.CharField(max_length=100)
    driver = models.ForeignKey('Driver')

    def __unicode__(self):
        #d = u'[{}]'.format(u','.join(map(unicode, self.dishes.all())))
        d = 'TODO'
        return u'<DeliveryOrder: Customer={} Address={} Dishes={}>'.format(
            self.customerName, self.deliveryAddress, d
        )

class Employee(models.Model):
    name = models.CharField(max_length = 30)
    enabled = models.BooleanField(default=True)

class Server(Employee):

    def __unicode__(self):
        return u'<Server: ({}) Name={}>'.format(self.id, self.name)

class Driver(Employee):
    carRegistration = models.CharField(max_length=8)

    def __unicode__(self):
        return u'<Driver: ({}) Name={} Reg={}>'.format(self.id,
                                                       self.name,
                                                       self.carRegistration
                                                      )
