from rest_framework.test import APITestCase
from rest_framework import status

import models
import test_helper

class DeliveryOrderTests(APITestCase,
                      test_helper.ListGetMixin,
                      #test_helper.UpdateMixin,
                      test_helper.DestroyMixin):
    model = models.DeliveryOrder
    list_url = '/delivery/'
    update_url = '/delivery/1/'
    update_data = {'customerName': 'Name Updated',
                   'deliveryAddress': 'Address Updated',
                   'dishes':[1,2]}
    update_check_models = False
    destroy_url = '/delivery/1/'
    destroy_pk = {'id': 1}

    def setUp(self):
        models.Dish(description='curry',
                    price=400,
                    vegetarian=False
                   ).save()
        models.Dish(description='chips',
                    price=300,
                    vegetarian=True
                   ).save()
        o1 = models.DeliveryOrder(
            customerName = 'N1',
            deliveryAddress = 'A1'
        )
        o1.save()
        o2 = models.DeliveryOrder(
            customerName = 'N2',
            deliveryAddress = 'A2'
        )
        o2.save()
        o2.dishes.add(models.Dish.objects.get(id=1))
        o2.save()
        o3 = models.DeliveryOrder(
            customerName = 'N3',
            deliveryAddress = 'A3'
        )
        o3.save()
        map(o3.dishes.add, models.Dish.objects.all())
        o3.save()

    def test_unicode(self):
        self.assertEquals(
            unicode(self.get_model().objects.get(id=1)),
            u'<DeliveryOrder: Customer=N1 Address=A1 Dishes=[]>'
        )
        dish = models.Dish.objects.get(id=1)
        self.assertEquals(
            unicode(self.get_model().objects.get(id=2)),
            u'<DeliveryOrder: Customer=N2 Address=A2 Dishes=[{}]>'.format(dish)
        )
        dish = u','.join(map(unicode, models.Dish.objects.all()))
        self.assertEquals(
            unicode(self.get_model().objects.get(id=3)),
            u'<DeliveryOrder: Customer=N3 Address=A3 Dishes=[{}]>'.format(dish)
        )

    def test_list_create(self):
        # Empty
        response = self.client.post('/delivery/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'customerName': [u'This field is required.'],
                          'deliveryAddress': [u'This field is required.']}
                        )

        # Blank fields
        response = self.client.post('/delivery/',
                                    {'customerName': '',
                                     'deliveryAddress':''}
                                   )
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['customerName'],
                         [u'This field may not be blank.']
                        )
        self.assertEqual(response.data['deliveryAddress'],
                         [u'This field may not be blank.']
                        )

        # Valid
        response = self.client.post('/delivery/',
                                    {'customerName': 'N4',
                                     'deliveryAddress': 'A4',
                                     'dishes': [1,2]}
                                   )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(self.get_model().objects.all()), 4)
