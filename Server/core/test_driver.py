from rest_framework.test import APITestCase
from rest_framework import status

import models
import test_helper

class DriverTests(APITestCase,
                      test_helper.ListGetMixin,
                      test_helper.UpdateMixin,
                      test_helper.DestroyMixin):
    model = models.Driver
    list_url = '/driver/'
    update_url = '/driver/1/'
    update_data = {'name': 'Name Updated',
                   'carRegistration': 'Reg Upd'}
    destroy_url = '/driver/1/'
    destroy_pk = {'id': 1}

    def setUp(self):
        self.get_model()(name='D1',
                        carRegistration='R1').save()
        self.get_model()(name='D2',
                        carRegistration='R2').save()
        self.get_model()(name='D3',
                        carRegistration='R3').save()

    def test_unicode(self):
        self.assertEquals(
            unicode(self.get_model().objects.get(id=1)),
            u'<Driver: (1) Name=D1 Reg=R1>'
        )
        self.assertEquals(
            unicode(self.get_model().objects.get(id=2)),
            u'<Driver: (2) Name=D2 Reg=R2>'
        )
        self.assertEquals(
            unicode(self.get_model().objects.get(id=3)),
            u'<Driver: (3) Name=D3 Reg=R3>'
        )

    def test_list_create(self):
        # Empty
        response = self.client.post('/driver/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'name': [u'This field is required.'],
                          'carRegistration': [u'This field is required.']}
                        )

        # Blank fields
        response = self.client.post('/driver/',
                                    {'name': '',
                                     'carRegistration': ''}
                                   )
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['name'],
                         [u'This field may not be blank.']
                        )
        self.assertEqual(response.data['carRegistration'],
                         [u'This field may not be blank.']
                        )

        # Reg too long
        response = self.client.post('/driver/',
                                    {'name': 'D4',
                                     'carRegistration': '123456789'}
                                   )
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['carRegistration'],
                         [u'Ensure this field has no more than 8 characters.']
                        )

        # Valid
        response = self.client.post('/driver/',
                                    {'name': 'D4',
                                     'carRegistration': 'R4'}
                                   )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(self.get_model().objects.all()), 4)
