import locale

from rest_framework.test import APITestCase
from rest_framework import status

import models
import test_helper

class DishTests(APITestCase,
                test_helper.ListGetMixin,
                test_helper.UpdateMixin,
                test_helper.DestroyMixin):
    model = models.Dish
    list_url = '/dishes/'
    update_url = '/dishes/1/'
    update_data = {'price': 450}
    destroy_url = '/dishes/1/'
    destroy_pk = {'id': 1}

    def setUp(self):
        models.Dish(description='curry',
                    price=400,
                    vegetarian=False).save()
        models.Dish(description='chips',
                    price=300,
                   vegetarian=True).save()

    def test_unicode(self):
        dish = models.Dish(description = "curry",
                           price = 400000,
                           vegetarian = True)
        currency = locale.localeconv()['currency_symbol'].decode(locale.getlocale()[1])
        self.assertEquals(
            unicode(dish),
            u'<Dish: curry {}4,000.00 (v)>'.format(currency)
        )

    def test_list_create(self):
        # Empty
        response = self.client.post('/dishes/', {}, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'price': [u'This field is required.'],
                          'description': [u'This field is required.']}
                        )
        # Invalid price
        for price in [-1, 100001]:
            response = self.client.post('/dishes/',
                                        {'price': price,
                                         'description':''}
                                       )
            self.assertEqual(response.status_code,
                             status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data['price'],
                             [u'This field must be in the range 0-100,000.']
                            )
        # Blank description
        self.assertEqual(response.data['description'],
                             [u'This field may not be blank.']
                            )
        response = self.client.post('/dishes/',
                                        {'price': 100,
                                         'description': 'curry'}
                                       )
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(len(models.Dish.objects.all()), 3)
