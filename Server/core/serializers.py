import models
from rest_framework import serializers

from collections import Counter

class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dish
        fields = ('id', 'description', 'price', 'vegetarian')

    def validate_price(self, data):
        if data < 0 or data > 100000:
            raise serializers.ValidationError(
                "This field must be in the range 0-100,000."
            )
        return data

class DishCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DishCount
        fields = ('dish', 'count')

class DishIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dish
        fields = ('id')

class OrderSerializer(serializers.ModelSerializer):
    dishes = serializers.CharField(max_length=200)

    def validate_dishes(self, data):
        data = Counter(data.replace(' ','').split(','))
        try:
            for i in data:
                i = int(i)
                dish = models.Dish.objects.get(id=i)
        except (ValueError, models.Dish.DoesNotExist):
            raise serializers.ValidationError(
                "This field must be a valid dish id."
            )
        return data

    def create_counts(self, validated_data):
        counts = []
        for (dishId, count) in validated_data['dishes'].items():
            dish = models.Dish.objects.get(id=dishId)
            dishCount = models.DishCount(dish=dish, count=count)
            dishCount.save()
            counts.append(dishCount)
        return counts

class SitInOrderSerializer(OrderSerializer):
    class Meta:
        model = models.SitInOrder
        fields = ('id', 'table', 'dishes', 'server')

    def validate_table(self, data):
        if data < 0 or data > 10:
            raise serializers.ValidationError(
                "This field must be in the range 0-10."
            )
        return data

    def create(self, validated_data):
        counts = self.create_counts(validated_data)
        order = models.SitInOrder()
        order.table=validated_data['table']
        order.server = validated_data['server']
        order.save()
        for count in counts:
            order.dishes.add(count)
        order.save()
        return order

    def to_representation(self, order):
        rep = super(SitInOrderSerializer, self).to_representation(order)
        rep['dishes'] = ",".join(map(unicode, order.dishes.all()))
        return rep

class DeliveryOrderSerializer(OrderSerializer):
    class Meta:
        model = models.DeliveryOrder
        fields = ('id', 'customerName', 'deliveryAddress', 'dishes', 'driver')

    def create(self, validated_data):
        counts = self.create_counts(validated_data)
        order = models.DeliveryOrder()
        order.customerName=validated_data['customerName']
        order.deliveryAddress=validated_data['deliveryAddress']
        order.driver = validated_data['driver']
        order.save()
        for count in counts:
            order.dishes.add(count)
        order.save()
        return order

    def to_representation(self, order):
        rep = super(DeliveryOrderSerializer, self).to_representation(order)
        rep['dishes'] = ",".join(map(unicode, order.dishes.all()))
        return rep

class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Server
        fields = ('id', 'name')

class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Driver
        fields = ('id', 'name', 'carRegistration')
