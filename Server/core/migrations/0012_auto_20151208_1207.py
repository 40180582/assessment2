# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_auto_20151207_2056'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryorder',
            name='driver',
            field=models.ForeignKey(default=1, to='core.Driver'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sitinorder',
            name='server',
            field=models.ForeignKey(default=1, to='core.Server'),
            preserve_default=False,
        ),
    ]
