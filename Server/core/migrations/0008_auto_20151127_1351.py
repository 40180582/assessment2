# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20151127_1300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='dishCounts',
        ),
        migrations.AddField(
            model_name='order',
            name='dishes',
            field=models.ManyToManyField(to='core.Dish', null=True),
        ),
    ]
