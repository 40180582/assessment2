# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20151123_1706'),
    ]

    operations = [
        migrations.CreateModel(
            name='DishCount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField()),
                ('dish', models.ForeignKey(to='core.Dish')),
            ],
        ),
        migrations.AlterField(
            model_name='order',
            name='dishes',
            field=models.ManyToManyField(to='core.Dish', null=True),
        ),
    ]
