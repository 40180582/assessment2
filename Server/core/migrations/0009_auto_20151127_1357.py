# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20151127_1351'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='dishes',
        ),
        migrations.AddField(
            model_name='order',
            name='dishCounts',
            field=models.ManyToManyField(to='core.DishCount'),
        ),
    ]
