# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20151127_1237'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='dishes',
            new_name='dishCounts',
        ),
    ]
