# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deliveryorder',
            old_name='CustomerName',
            new_name='customerName',
        ),
        migrations.RenameField(
            model_name='deliveryorder',
            old_name='DeliveryAddress',
            new_name='deliveryAddress',
        ),
    ]
