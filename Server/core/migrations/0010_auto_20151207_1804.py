# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20151127_1357'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='dishCounts',
            new_name='dishes',
        ),
    ]
