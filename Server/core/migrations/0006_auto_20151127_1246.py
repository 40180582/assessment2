# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20151127_1241'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='dishCounts',
            field=models.ManyToManyField(to='core.DishCount'),
        ),
    ]
