# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dish',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=30)),
                ('price', models.IntegerField()),
                ('vegetarian', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='DeliveryOrder',
            fields=[
                ('order_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Order')),
                ('CustomerName', models.CharField(max_length=30)),
                ('DeliveryAddress', models.CharField(max_length=100)),
            ],
            bases=('core.order',),
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('employee_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Employee')),
                ('carRegistration', models.CharField(max_length=8)),
            ],
            bases=('core.employee',),
        ),
        migrations.CreateModel(
            name='Server',
            fields=[
                ('employee_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Employee')),
            ],
            bases=('core.employee',),
        ),
        migrations.CreateModel(
            name='SitInOrder',
            fields=[
                ('order_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Order')),
                ('table', models.IntegerField()),
            ],
            bases=('core.order',),
        ),
        migrations.AddField(
            model_name='order',
            name='dishes',
            field=models.ManyToManyField(to='core.Dish'),
        ),
    ]
